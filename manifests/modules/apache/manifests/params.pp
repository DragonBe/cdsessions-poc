class apache::params {

  $packages = [
    'apache2',
    'apache2-utils',
    'apache2-mpm-prefork'
  ]

  $vhosts = [
    'bastrucks',
    'basparts',
    'basmining',
    'account',
  ]

  $mods = [
    'env',
    'deflate',
    'headers',
    'rewrite',
    'ssl',
  ]

  $settings = [
    'ServerTokens Prod',
    'ServerSignature Off',
    'ServerName "testbox"'
  ]

}
