class apache::config {

  include apache::params
  include apache::config::vhosts
  include apache::config::mods
  include apache::config::settings

}
