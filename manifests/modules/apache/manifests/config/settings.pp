class apache::config::settings {

  include apache::params

  apache::config::set-settings { $apache::params::settings: }
}

define apache::config::set-settings () {

  $setting = "${name}"
  $config_file = '/etc/apache2/apache2.conf'
  exec { $setting:
      command => "echo $setting >> $config_file",
      unless => "grep -c \"$setting\" $config_file",
      require => Package['apache2'],
      notify => Service['apache2']
    }
}