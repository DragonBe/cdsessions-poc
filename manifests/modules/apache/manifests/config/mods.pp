class apache::config::mods {

  include apache::params

  apache::config::mods-installer {$apache::params::mods: }

}

define apache::config::mods-installer () {

  $activate = "a2enmod ${name}"
  exec { $activate:
    command => $activate,
    require => Package['apache2-utils','apache2'],
    unless => "find /etc/apache2/mods-enabled -print|grep -c ${name}",
    notify => Service['apache2']
  }

}
