class phpmyadmin::install {

  include phpmyadmin::params
  include phpmyadmin::install::packages

  $phpmyadminconf = '/etc/apache2/conf.d/phpmyadmin.conf'
  file { $phpmyadminconf:
    ensure => link,
    target => '/etc/phpmyadmin/apache.conf',
    require => Package['phpmyadmin'],
    notify => Service['apache2']
  }

  # Load configuration storage
  # See: http://docs.phpmyadmin.net/en/latest/setup.html#phpmyadmin-configuration-storage
  exec { "phpmyadmin-configuration-options":
    command => "zcat /usr/share/doc/phpmyadmin/examples/create_tables.sql.gz|mysql",
    require => Package['phpmyadmin'],
    unless => "mysql -e 'SHOW DATABASES;'|grep -c phpmyadmin",
    notify => Service["apache2"]
  }

  $pmapass = "gO5knVwyw265"
  exec { "update-control-user":
    command => "sed -i \"s/dbpass='[a-zA-Z0-9]+'/dbpass='$pmapass'/\" /etc/phpmyadmin/config-db.php",
    onlyif => "grep -c $pmapass /etc/phpmyadmin/config-db.php",
    require => Exec["phpmyadmin-configuration-options"]
  }

  exec { "add-phpmyadmin-control-user":
    command => "mysql -e \"CREATE USER 'phpmyadmin'@'localhost' IDENTIFIED BY '$pmapass'; GRANT GRANT SELECT, INSERT, UPDATE, DELETE ON phpmyadmin.* TO 'phpmyadmin'@'localhost' IDENTIFIED BY '$pmapass';\"",
    require => Exec["update-control-user"],
    unless => "mysql -e \"select user from mysql.user;\"|grep -c phpmyadmin"
  }
}