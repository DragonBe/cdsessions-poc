class php::install::tools {

  include php::params

  php::install::tool-installer { $php::params::tools: }

  define php::install::tool-installer () {

      $file = "install-${name}.sh"
      $path = "/tmp/${file}"

      file {$path:
        ensure => file,
        mode => 0755,
        source => "puppet:///modules/php/${file}"
      }

      exec {$file:
        command => "/bin/sh ${path}",
        unless => [
            "test -f /usr/local/bin/${name}",
            "test -f /usr/bin/${name}"
        ],
        require => [File[$path],Package['subversion','git','php5']]
      }
  }
}