#!/bin/sh

ZF="zf1"
URL="https://github.com/zendframework/zf1"
LIB=library/Zend
INCLUDE_PATH=/usr/share/php

cd /tmp
echo "Fetching $ZF"
git clone -q $URL /tmp/$ZF

echo "Installing $ZF"
cd /tmp/$ZF
mkdir -p $INCLUDE_PATH
cp -rp $LIB $INCLUDE_PATH/Zend

echo "Cleaning up"
rm -rf /tmp/$ZF