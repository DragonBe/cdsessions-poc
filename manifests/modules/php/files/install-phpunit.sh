#!/bin/sh

PHPUNIT="PHPUnit-3.4.15"
URL="http://in2itqa.cloudapp.net/get/$PHPUNIT.tgz"
INCLUDE_PATH=/usr/share/php

cd /tmp
echo "Fetching $PHPUNIT.tgz"
wget -q -O /tmp/$PHPUNIT.tgz $URL

echo "Unpacking $PHPUNIT"
tar -xzf $PHPUNIT.tgz

echo "Installing $PHPUNIT"
cd $PHPUNIT
mkdir -p $INCLUDE_PATH
cp -rp PHPUnit $INCLUDE_PATH/PHPUnit
cp phpunit.php /usr/local/bin/phpunit
cp dbunit.php /usr/local/bin/dbunit

echo "Cleaning up"
rm -rf /tmp/$PHPUNIT.tgz
rm -rf /tmp/$PHPUNIT