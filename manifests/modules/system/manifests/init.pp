class system {

  include system::params
  include system::install

  file {'/home/vagrant/.bashrc':
    ensure => file,
    owner => 'vagrant',
    group => 'vagrant',
    mode => 0600,
    source => 'puppet:///modules/system/bashrc'
  }
}
