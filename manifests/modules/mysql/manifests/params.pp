class mysql::params {

  $packages = [
    'mysql-client',
    'mysql-common',
    'mysql-server',
    'mysql-proxy'
  ]
}