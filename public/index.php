<?php
require_once 'init_sessions.php';
$token = generateIdentificationToken();
if (empty ($_SESSION)) {
    $memcache = new Memcache();
    $memcache->connect($memcache_host, $memcache_port);
    if (false === ($sessionId = $memcache->get($token))) {
        session_start();
        $sessionId = session_id();
        $memcache->set($token, $sessionId, false, 0);
    } else {
        session_id($sessionId);
        session_start();
    }
}
$items = array ('apple', 'banana', 'chocolate');
if (!empty ($_POST)) {
    foreach ($items as $item) {
        if (isset ($_POST[$item . '-quantity'])) {
            $_SESSION[$item] = (int) $_POST[$item . '-quantity'];
        }
    }
    header('Location: ' . $_SERVER['SCRIPT_URI']);
    exit;
}
?>
<html>
  <head><title>Hello World</title></head>
  <body>
    <p>Hello there, welcome to <?php echo $_SERVER['HTTP_HOST'] ?>.</p>
    <ul>
        <?php foreach ($hosts as $host): ?>
            <?php if ($host . '.local' === $domain): ?>
                <ul>www.<?php echo $host ?>.local</ul>
            <?php else: ?>
        <ul><a href="http://www.<?php echo $host ?>.local">www.<?php echo $host ?>.local</a></ul>
            <?php endif ?>
        <?php endforeach ?>
    </ul>
    <p>Current Session ID: <?php echo session_id() ?></p>
    <h2>Basket</h2>
    <ul>
        <?php foreach ($_SESSION as $name => $quantity): ?>
            <li><?php echo $name ?>:<?php echo $quantity ?></li>
        <?php endforeach ?>
    </ul>
    <h2>Shop</h2>
    <form name="shop" action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
        <ul>
            <?php foreach ($items as $product): ?>
                <li><?php echo $product ?><input type="text" name="<?php echo $product ?>-quantity" size="4" value="<?php echo (isset ($_SESSION[$product]) ? $_SESSION[$product] : 1)?>"></li>
            <?php endforeach ?>
        </ul>
        <input type="submit" value="order">
    </form>
    <h2>Current token</h2>
    <pre><?php echo implode(PHP_EOL, array ($token, getUserAgent(), getRealIp())) ?></pre>
  </body>
<html>
