<?php
if (!defined('SESS_SALT')) {
    define('SESS_SALT', 'Horse Mountain Fish Truck');
}
preg_match('/^www\.(([^\.]+)\.local)$/', $_SERVER['HTTP_HOST'], $match);
$domain = strtolower($match[1]);
$SESSID = strtoupper($match[2] . 'ID');
$memcache_host = '127.0.0.1';
$memcache_port = 11211;
ini_set('session.save_handler', 'memcache');
ini_set('session.save_path', 'tcp://' . $memcache_host . ':' . $memcache_port);
ini_set('session.cookie_domain', $domain);
ini_set('session.cookie_path', '/');
ini_set('session.cookie_secure', false);
ini_set('session.cookie_httponly', false);
ini_set('session.name', $SESSID);

$hosts = array ('basparts', 'bastrucks', 'basmining');

function generateIdentificationToken()
{
    $ip = getRealIp();
    $ua = getUserAgent();
    return sha1($ip . ' - ' . $ua . SESS_SALT);
}

function getRealIp()
{
    $options = array (
        'HTTP_CLIENT',
        'HTTP_X_FORWARDED_FOR',
        'HTTP_X_FORWARDED',
        'HTTP_FORWARDED_FOR',
        'HTTP_FORWARDED',
        'REMOTE_ADDR',
    );
    $ipAddress = null;
    foreach ($options as $option) {
        if (!isset ($_SERVER[$option])) continue;
        $ipAddress = $_SERVER[$option];
        $validate = filter_var($ipAddress, FILTER_VALIDATE_IP);
        if (false !== $validate) {
            return $ipAddress;
        }
    }
    if (null === $ipAddress) {
        throw new RuntimeException('Could not detect correct IP Address');
    }
}

function getUserAgent()
{
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
    return $userAgent;
}