# POC - Cross Domain Sessions

This is a proof-of-concept where you can test-drive multi-domain session management. 

See the [screencast](https://www.dropbox.com/s/wsufwjiqq10ga3j/cross-domain%20sessions.mov?dl=0)

**WARNING:** this is not production-ready code! Do not use in production!!!

## Installation

Installation is easy:

    git clone git@bitbucket.org:DragonBe/cdsessions-poc.git
    cd cdsessions-poc/
    vagrant up

You need to modify your host file and add the following entries

    192.168.205.205 bastrucks.local www.bastrucks.local
    192.168.205.205 basparts.local www.basparts.local
    192.168.205.205 basmining.local www.basmining.local
    192.168.205.205 account.local www.account.local

## Test drive it

Just point your browser to [www.bastrucks.local](http://www.bastrucks.local) and watch the magic happen.
